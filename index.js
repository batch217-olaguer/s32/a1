let http = require("http");

let port = 4000;

let server = http.createServer((req, res) => {
		// HTTP method - get
		// get method means that we will be retrieving or reading information.

		// If the url is http://localhost:4000/, send a response Welcome to Booking System - GET
	if(req.url == "/" && req.method == "GET" ){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to the Booking System.");

		// If the url is http://localhost:4000/profile, send a response Welcome to your profile! -GET
	} else if(req.url == "/profile" && req.method == "GET" ){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to your profile!");

		// If the url is http://localhost:4000/courses, send a response Here’s our courses available - GET
	}  else if(req.url == "/courses" && req.method == "GET" ){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Here's our available courses.");

		// If the url is http://localhost:4000/addcourse, send a response Add a course to our resources - POST
	}  else if(req.url == "/addCourse" && req.method == "POST" ){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Add courses to our resources.");

		// If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources - PUT
	}  else if(req.url == "/updatecourse" && req.method == "PUT" ){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update a course at from our resources.");

		// If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources - DELETE
	}  else if(req.url == "/archivecourses" && req.method == "DELETE" ){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Archive courses at our resources.");
	} else {
		response.writeHead(404, {"Content-Type": "text-plain"});
		response.end("I'm sorry, the page you are looking for cannot be found.");
	}
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);